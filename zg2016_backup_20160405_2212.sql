-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas generowania: 05 Kwi 2016, 22:11
-- Wersja serwera: 5.6.27-0ubuntu1
-- Wersja PHP: 5.6.11-1ubuntu3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `zg2016`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `application`
--

CREATE TABLE IF NOT EXISTS `application` (
  `id` int(11) NOT NULL,
  `team_id` int(11) DEFAULT NULL,
  `count` int(11) NOT NULL,
  `total_count` int(11) NOT NULL,
  `amount_per_one` decimal(10,2) NOT NULL,
  `total_amount` decimal(10,2) NOT NULL,
  `advance` decimal(10,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `z` int(11) DEFAULT NULL,
  `h` int(11) DEFAULT NULL,
  `hs` int(11) DEFAULT NULL,
  `w` int(11) DEFAULT NULL,
  `i` int(11) DEFAULT NULL,
  `o` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `application`
--

INSERT INTO `application` (`id`, `team_id`, `count`, `total_count`, `amount_per_one`, `total_amount`, `advance`, `created`, `modified`, `z`, `h`, `hs`, `w`, `i`, `o`) VALUES
(1, 1, 10, 10, 165.00, 1650.00, 990.00, '2016-02-29 21:56:07', '2016-02-29 21:56:07', 0, 6, 2, 0, 1, 1),
(2, 3, 21, 21, 165.00, 3465.00, 2079.00, '2016-02-29 22:37:46', '2016-02-29 22:37:46', 0, 4, 10, 6, 0, 1),
(3, 6, 4, 4, 165.00, 660.00, 396.00, '2016-03-02 01:03:33', '2016-03-02 01:03:33', 0, 4, 0, 0, 0, 0),
(4, 5, 19, 19, 165.00, 3135.00, 1881.00, '2016-03-06 18:21:58', '2016-03-06 18:21:58', 0, 11, 1, 5, 1, 1),
(5, 8, 15, 15, 165.00, 2475.00, 1485.00, '2016-03-09 16:21:14', '2016-03-09 16:21:14', 0, 0, 11, 4, 0, 0),
(6, 11, 10, 10, 165.00, 1650.00, 990.00, '2016-03-14 11:05:01', '2016-03-14 11:05:01', 0, 6, 1, 2, 1, 0),
(7, 33, 5, 5, 165.00, 825.00, 495.00, '2016-03-25 22:31:55', '2016-03-25 22:31:55', 4, 0, 0, 0, 0, 1),
(8, 36, 18, 18, 165.00, 2970.00, 1782.00, '2016-03-27 20:59:07', '2016-03-27 20:59:07', 0, 0, 3, 14, 1, 0),
(9, 30, 22, 22, 165.00, 3630.00, 2178.00, '2016-03-28 20:30:57', '2016-03-28 20:30:57', 0, 0, 4, 15, 2, 1),
(10, 38, 7, 7, 165.00, 1155.00, 693.00, '2016-03-29 08:56:04', '2016-03-29 08:56:04', 2, 3, 1, 0, 1, 0),
(11, 35, 10, 10, 165.00, 1650.00, 990.00, '2016-03-29 21:42:50', '2016-03-29 21:42:50', 0, 6, 0, 3, 0, 1),
(12, 14, 25, 25, 165.00, 4125.00, 2475.00, '2016-03-30 09:49:13', '2016-03-30 09:49:13', 2, 9, 9, 5, 0, 0),
(13, 42, 11, 11, 165.00, 1815.00, 1089.00, '2016-03-30 14:31:52', '2016-03-30 14:31:52', 7, 0, 0, 4, 0, 0),
(14, 12, 8, 8, 165.00, 1320.00, 792.00, '2016-03-30 18:32:06', '2016-03-30 18:32:06', 0, 5, 0, 2, 1, 0),
(15, 40, 27, 27, 165.00, 4455.00, 2673.00, '2016-03-30 18:43:14', '2016-03-30 18:43:15', 22, 0, 1, 1, 2, 1),
(16, 31, 12, 12, 165.00, 1980.00, 1188.00, '2016-03-30 19:08:34', '2016-03-30 19:08:34', 9, 1, 0, 0, 0, 2),
(17, 39, 4, 4, 165.00, 660.00, 396.00, '2016-03-30 20:46:17', '2016-03-30 20:46:17', 0, 0, 0, 4, 0, 0),
(18, 37, 19, 19, 165.00, 3135.00, 1881.00, '2016-03-30 23:03:18', '2016-03-30 23:03:18', 0, 2, 4, 11, 0, 2),
(19, 43, 13, 13, 165.00, 2145.00, 1287.00, '2016-03-30 23:17:14', '2016-03-30 23:17:14', 0, 0, 10, 2, 0, 1),
(20, 41, 6, 6, 165.00, 990.00, 594.00, '2016-03-31 14:13:09', '2016-03-31 14:13:09', 0, 0, 3, 2, 1, 0),
(21, 45, 7, 7, 165.00, 1155.00, 693.00, '2016-03-31 19:29:04', '2016-03-31 19:29:04', 4, 0, NULL, 2, 0, 1),
(22, 34, 12, 12, 165.00, 1980.00, 1188.00, '2016-03-31 19:57:43', '2016-03-31 19:57:43', 0, 4, 2, 5, NULL, 1),
(23, 46, 15, 15, 165.00, 2475.00, 1485.00, '2016-03-31 22:02:02', '2016-03-31 22:02:02', 0, 4, 8, 2, 1, 0),
(24, 47, 7, 7, 165.00, 1155.00, 693.00, '2016-03-31 22:11:45', '2016-03-31 22:11:45', 6, 0, 0, 0, 1, 0),
(25, 32, 10, 10, 165.00, 1650.00, 990.00, '2016-03-31 22:50:42', '2016-03-31 22:50:42', 0, 8, 0, 1, 1, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dict_ensign`
--

CREATE TABLE IF NOT EXISTS `dict_ensign` (
  `id` int(11) NOT NULL,
  `ensign_name` varchar(120) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `dict_ensign`
--

INSERT INTO `dict_ensign` (`id`, `ensign_name`) VALUES
(1, 'Białostocka'),
(2, 'Dolnośląska'),
(3, 'Gdańska'),
(4, 'Kielecka'),
(5, 'Krakowska'),
(6, 'Kujawsko-Pomorska'),
(7, 'Łódzka'),
(8, 'Lubelska'),
(9, 'Mazowiecka'),
(10, 'Opolska'),
(11, 'Podkarpacka'),
(12, 'Śląska'),
(13, 'Stołeczna'),
(14, 'Warmińsko-Mazurska'),
(15, 'Wielkopolska'),
(16, 'Zachodniopomorska'),
(17, 'Ziemi Lubuskiej'),
(18, 'Główna Kwatera ZHP'),
(19, 'Inne');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `fos_user`
--

CREATE TABLE IF NOT EXISTS `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `name`, `surname`, `phone`, `info`) VALUES
(1, 'dorota.olszewska', 'dorota.olszewska', 'dorota.olszewska@zhp.net.pl', 'dorota.olszewska@zhp.net.pl', 1, '7c5hdxglyg4kgw4gckcgs44wss8c44w', 'L9Dc9cMvSE4R+IbU+M1Swd3Cn+Uarq3qJz7BlqCpbz9fNDAQqBMOvWXuUSbqbBt5zGbQGrHZawPENI5Ho1PmxQ==', '2016-04-05 21:47:18', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:8:"ROLE_PAY";}', 0, NULL, 'Dorota', 'Jeżowska-Olszewska', '605829739', NULL),
(2, '4GDDH', '4gddh', 'grzegorz.przybylak@zhp.net.pl', 'grzegorz.przybylak@zhp.net.pl', 1, '2hu03yfm1xc0kssogs04w4cccgk4skk', 'DDEjrVhg9f2SKjjZg1HWWJeDo+kJ+vpkfsLrQPc3jky580+YuPxFFQyK/4Asarl9412/uZgL2LpJwMFKtF4Tag==', '2016-03-03 20:41:52', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Grzegorz', 'Przybylak', '601922979', NULL),
(3, 'Dominika_Golon', 'dominika_golon', 'Dominika_Golon@op.pl', 'dominika_golon@op.pl', 1, 'ahteztlo0mww0k8o4o44w0csc4owcgc', 'HKuiyJW73CfAn4+A0Rq9LLP2hphm/cUZ8EQjogVx1p6Du3Eam4IEPKef6hUjIoK7gmslntDmJU/xCoxiPpaAbA==', '2016-03-09 20:31:43', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Dominika', 'Golon', '728518643', NULL),
(4, 'Antidotum34', 'antidotum34', 'martyna_pien@o2.pl', 'martyna_pien@o2.pl', 1, 'o091wfnzujkkwwkwcw0gw0okw84w8os', 'OX5QVuHiJLGg9oNpK9ksDTbib9xqP9+yjCPVC+PfMK49WefZvbVUCfEuK8ddqW7G1nA2xHaRGUFHaXR5qRnqQg==', '2016-02-29 22:34:29', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Martyna', 'Pieniek', '515046524', NULL),
(5, 'Matys', 'matys', 'mateusz.slaski@kielecka.zhp.pl', 'mateusz.slaski@kielecka.zhp.pl', 1, '6m5f2yxx98wsokcc44cokgswookkcg0', 'rRhbD413qcX12pqiXy/RFSTB4i40QsMElWQfdw8zmKNHi6Mg7NbJCrs1EJmOQMG67NvUUaPE8fwRsu9ozght9w==', '2016-02-29 22:45:50', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Mateusz', 'Ślaski', '600260634', NULL),
(6, 'Monia', 'monia', 'monika.chmiel@zhp.net.pl', 'monika.chmiel@zhp.net.pl', 1, '20tknypfsm80scskookgwgckogks08s', '7h/HplN0P2GonJzJfoQXZkLcZPrFSw7fNLIp36ZIe9wA9CxLuaVkGblEoa8+hS0z26z74rGrahpI6DQBmfhIwA==', '2016-03-29 09:46:22', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:8:"ROLE_PAY";}', 0, NULL, 'Monika', 'Chmiel', '605826866', 'Hufiec Orneta, Chorągiew Warmińsko-Mazurska\r\nZ-ca Komendanta Chorągwi ds. programowych'),
(7, 'Tomasz', 'tomasz', 'kaktustw@wp.pl', 'kaktustw@wp.pl', 1, 'j10cj1dsr5cs8ogwk0w8wowc0ssgs0w', 'maSwiV6l2qIGbS+cgXqrCQZScJ7wiyAys38sY2lfwFaKvf8lrxQBz0sCV4E2ekzp3KLVigIy5MqVDpSGlfDbBw==', '2016-03-01 10:01:37', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Tomasz', 'Weber', '517117550', NULL),
(8, 'gdhwielichowo', 'gdhwielichowo', 'aleksandramonikamichalska@gmail.com', 'aleksandramonikamichalska@gmail.com', 1, 'maald0vdpeok0wc8w48k08kcws0sgk0', '9ynYRhkC2QwMkmYSZssB6PDLOII4QOwmvvEasZmvzVAuYq+bH2MxtDsJAwJ7QnA1lQrWBr4Omytqz0zCfN1CRA==', '2016-03-31 22:24:31', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Aleksandra', 'Michalska', '664285428', 'Chorągiew Wielkopolska, \r\nHufiec ZHP Śmigiel, \r\nDrużynowa 9 GDH " Buki " im. Kazimierza Wielkiego w Wielichowie'),
(9, 'bart9702', 'bart9702', 'bart9702@gmail.com', 'bart9702@gmail.com', 1, 'i3fepmm3km8gkkw8k8wgkksw04skkw0', '+Ajmtnk/5wGJ3y7qe+BeX8/3GLOAbbTAJvzRLZcE3bs2NQ0oVC+0SyGZbp7Q1jplLYWLDQv+26LGRZob4qSpxg==', '2016-03-01 13:08:30', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, NULL, NULL, NULL, NULL),
(10, 'Maciejilawa', 'maciejilawa', 'ilawa@zhp.pl', 'ilawa@zhp.pl', 1, '9umhp9whdlog0g44sww480k84gcoc0o', '9vNCs6MKWdEDthBAFMNETVpRaCuHBi4XjbVOcWHd8NVa/S6k4Robl3+6p43xW1ibTgTZCaL7Sh8neGmMkCrLBg==', '2016-03-02 00:59:02', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Maciej', 'Marciniak', 'Maciejilawa', NULL),
(11, 'plonson', 'plonson', 'plopaw@wp.pl', 'plopaw@wp.pl', 1, 'ikn8uihv9p4co84444osg0ossccsc00', 'Lj8q80UJ2uJr6KZl9oeVOVYwm1d+0WPJoGKJKT7jLJGEPJfgK+eiCKu/OJz29z2NWjw7bhVg6CHP7ORoRLE10Q==', '2016-03-02 11:42:20', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Paweł', 'Plona', '509656534', NULL),
(12, '501PDHAranea', '501pdharanea', 'martaolszewska93@wp.pl', 'martaolszewska93@wp.pl', 1, '6h09348kzksg0s8k8sc0ckg80sc80cg', 'P/+7HkfWffcceZgV29tkbmLS5efYXNcqBG4renW8kVuGa2UgcQwr2qbofSs1HHHhbGjmPto8YwRgews3YoudXw==', '2016-03-02 16:09:03', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Marta', 'Olszewska', '601260661', NULL),
(13, 'wrgsh11', 'wrgsh11', 'wrgsh11@zhp.net.pl', 'wrgsh11@zhp.net.pl', 1, 'a0axnyou9lskcwoogcw8sg4og4ow4sw', '1qCn9ciKSfKExDJ6KenQvu3z3GUuUBiCKMA0g7ZgLkP67aCG9HNvGD/H2jvbZpMpKZDQ8PuK6qBvdco4ZZRhJw==', '2016-03-03 19:38:00', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, NULL, NULL, NULL, NULL),
(14, 'Damian', 'damian', 'wojdadamian@gmail.com', 'wojdadamian@gmail.com', 1, 'km17fozdvtw4wg48ogcosks84k8os8k', 'uOEuttjxzpUd6GqOLmDidKJOJNRlJWMtvDXomgaBiglJDNFa23xZRTiJj76yjSFVah2HEpsFqmag8oYSlyR+qA==', '2016-03-02 21:19:11', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, NULL, NULL, NULL, NULL),
(15, 'IIpdh', 'iipdh', 'irena.grzela@wp.pl', 'irena.grzela@wp.pl', 1, 'dd3orv9hrzks44s88s0kk84w888wccw', '89zJAizcyl7c9sIy31ULgPu+WUtomjtYCyySOd5oc6Qqq9XiG6DA1oehMSaJPk6sx5JiT8web0zuB3/7TMh2tg==', '2016-03-29 21:41:37', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Irena', 'Grzela', '694628695', 'Hufiec Piotrków Trybunalski\r\nChorągiew Łódzka \r\nDruzynowa II PDH "Sulima" im. Zawiszy Czarnego'),
(16, 'Pojmen', 'pojmen', 'abbapojmen@gmail.com', 'abbapojmen@gmail.com', 1, 'bxelzz2bfrwwk4gk80g840csw00c8w0', 'PfhLLcwolxqf70PoymeI7AnsxgxhLx6SKUOIOvZDaQqoG5879c8C3XKcXuE43NUqfLfneqO9Xu3ENFwokeSbTw==', '2016-03-28 20:25:56', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'ks. Łukasz', 'Rybiński', '535776354', 'Ostrołęka\r\nChorągiew Mazowiecka\r\ndrużynowy'),
(17, 'Barbara Kałapus', 'barbara kałapus', 'bkalapus@gmail.com', 'bkalapus@gmail.com', 1, 'm9wq5rwwgf4g080k0wosoosk8w8ocso', '6LfYoSpyCsKdYDO52MMCMPnsbhX5DDeNTDwBT9WM1s6DuhC1x5L5Tx/QFoEA/y+Py3BL1KMM0UJA1zwoJym8+A==', '2016-04-03 16:21:02', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Barbara', 'Kałapus', '502207098', 'Hufiec ZHP Mława, chorągiew mazowiecka, drużynowa'),
(18, 'piszu', 'piszu', 'kontakt@mariuszpisz.pl', 'kontakt@mariuszpisz.pl', 1, '9eu18a7j3ksgog0cwccck808sgokcos', 'PyMAR71z5zISsg84cuLlEztt7nG581MRPkNWbcLOoPJOCxQ8MLsCbDqN1H7Wx6bA0zLgtpFf8Lo+97fxsU+LyA==', '2016-04-01 13:08:48', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:8:"ROLE_PAY";}', 0, NULL, 'Mariusz', 'Pisz', '501804197', NULL),
(19, 'Bastionowo', 'bastionowo', 'magdalenanawrocka@wp.pl', 'magdalenanawrocka@wp.pl', 0, 'icgr1k28iw0g4cgwo8wcgc48ow4ww8w', 'VT9paUgvR/9aa0TpCNFoZLnp2h8vY51VIIXTUw4T6secHrqbu2QDro0AsTgaX0Su2lisGI8MJ+8lYgQnV7Gmgw==', NULL, 0, 0, NULL, 'm0Sit8ulHhncKrxCjYRjDU__vt3Nq2OUjkpF5RCvNDk', NULL, 'a:0:{}', 0, NULL, NULL, NULL, NULL, NULL),
(20, 'Bastion', 'bastion', 'magdalenanawrocka540@wp.pl', 'magdalenanawrocka540@wp.pl', 1, 'bq5iia269fk0cksk44w88w0oo044o84', 'TVeD0IsTRwZEECDfQSjyAe21zaXr9htBvjhSV1IM/UtRkNI4A1PfOfyF3+YBeN8c6B6fdrUQMI93bxahgBnZAg==', '2016-03-08 16:13:14', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Magdalena', 'Nawrocka', '660 823 247', NULL),
(21, 'Dagmara96', 'dagmara96', 'dagmara.kamrowska@gmail.com', 'dagmara.kamrowska@gmail.com', 1, 'h3fwn0896z4800sg44sk0o80ocgwokg', 'gSdAFYmNFLDzB05qJXLtn5BqLxER1AFjEsfiVBbWqxknFqN0i4C1sgF5berrRevjX4TO3keB7PoWP1FVhj3eSg==', '2016-03-08 19:08:50', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, NULL, NULL, NULL, NULL),
(22, 'Emilka', 'emilka', '8_OGDH_wilki@wp.pl', '8_ogdh_wilki@wp.pl', 1, '31dj0xxyibwgossggw8gs8c88wokk40', 'j0zLL+eHvOSuHvC6s0V7y+IuduFByvd8V5o4oFJoa04MeF3weMMuD9SE27NXN95L9SCEtGAjNFqwIM+Rjd0XYA==', '2016-03-09 18:24:27', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, NULL, NULL, NULL, NULL),
(23, 'mieczyslaw.kaleciak', 'mieczyslaw.kaleciak', 'mieczyslaw.kaleciak@zhp.net.pl', 'mieczyslaw.kaleciak@zhp.net.pl', 1, 'q6mq0tjrguss40s4sg8kowg8swkwcc4', 'LH5r5SxmGz5l4bE5Bnl2qFWdPbIkKymUI0BwbKHkLo2brl5p774oab9+kYAaz8t6zFAGBXdW9awXY4KR5VFw4g==', '2016-04-05 18:53:30', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Mieczysław', 'Kaleciak', '697785640', 'Świecie-Powiat, Chorągiew Kujawsko-Pomorska ZHP, Drużynowy 26 GŚDH Lisowczyczy, członek komisji rewizyjnej'),
(24, '14 GDH "Puszczyki"', '14 gdh "puszczyki"', 'kaluska.anna@gmail.com', 'kaluska.anna@gmail.com', 1, 'aztv8ynq70o4c44sck4w8c4sso0kg0s', 'QPu95Fh/KGIK3qZc9OOa+bef+rb5wSpbhYjQiV+QsXGo4nX0mgqvu6I2e+SzKwhbFFnTnEXZsnREfNHG99+FCQ==', '2016-03-31 18:51:39', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Anna', 'Kałuska', '666301731', 'Hufiec Wołomin, Chorągiew Stołeczna, Drużynowa 14 GDH "Puszczyki"'),
(25, '26 GWODH ,,Cassiopea"', '26 gwodh ,,cassiopea"', '26gwodh@wp.pl', '26gwodh@wp.pl', 1, '31kjh8jx4kcgkwsoscs4ssg4cs8woso', 'Jt+gIiEoesphwMj+Or+DXvReHRN+467T9NeWgUukVNCrwY1z443nk8+Q+XNuataRb79iU0cJbt6i3zdjoBO2Cw==', '2016-03-30 23:02:14', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Karolina', 'Zagórska', '723298012', 'Hufiec Warmiński\r\nChorągiew Warmińsko-Mazurska\r\nPrzyboczna'),
(26, 'Monika Palczewska', 'monika palczewska', 'monikawszeborowska@vp.pl', 'monikawszeborowska@vp.pl', 1, 'avo9nksjql4c0ko8c8cw84c8wg8sock', 'lS0Tq2yPAIOC9eTRTQO3DZv/WDyK0Hfxv7V8h0Lbt0tlTki2pFcqnYbudh6zJLO1uMIVFJgZwDiD8XbIAVi0iQ==', '2016-04-02 19:27:12', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Monika', 'Palczewska', '508688178', 'Hufiec ZHP Pisz im. Janusza Korczaka, Chorągiew Warmińsko- Mazurska,'),
(27, 'katarzyna konopka', 'katarzyna konopka', 'kasi@poczta.onet.eu', 'kasi@poczta.onet.eu', 1, '53dkgqsnvy80w84ks0w440koo4kkkc8', 'hTtJuJKSVBuMG3Dc5fuC3UXH+iL5tP/drnTKVmx08PK+TWudVcjrDesrzB2t6a/VH9pl8YwtbQdtTzfd5Tl/YA==', '2016-03-14 20:36:39', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'katarzyna', 'konopka', '662090975', NULL),
(28, 'Cieszyn_Berbeleszyn', 'cieszyn_berbeleszyn', 'kuba.cieszynski@wp.pl', 'kuba.cieszynski@wp.pl', 1, 'ckoidm1ib4g8ksoc044ow040csckk0g', '2sXsMtvwHDVeph7t4O2QFyJwWEHaypAAesJxprDzTokTFWfQ5CoX0KvY/h6gWH2XqC939Y4vYbgctBnpiEvEiw==', '2016-03-14 20:59:27', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Jakub', 'Cieszyński', '795194166', NULL),
(29, 'Julia Turowska', 'julia turowska', 'julia.turowska@o2.pl', 'julia.turowska@o2.pl', 1, '4z0a8adcsxs0wows0kcocw4kw4gwwcw', 'd9VuE3jidpbs4v4GjHHkenRnmTUjTiO/YEku9jmxrd18tZEGhvZrnsXQsP9mwQCziZ1e+0Fvpqk9giv8YEMaJg==', '2016-03-15 20:16:33', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, NULL, NULL, NULL, NULL),
(30, 'Agnieszka Wieczorek', 'agnieszka wieczorek', 'agnieszkawieczorek700@interia.pl', 'agnieszkawieczorek700@interia.pl', 1, 'dzgsbpexym80w4go4kcc0cc0s4484ws', '3BdPUdoEnJFJjgIMGqaLsyIEwtIRCZUvCLtthfEF1RwZJkBsY1F4DVbDcmf87NCuZV7ZBBNgYtWND6yWOhUucg==', '2016-03-18 19:59:48', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Agnieszka', 'Wieczorek', NULL, 'Chorągiew WM\r\nHufiec ZHP Orneta\r\nDryżynowa'),
(31, 'Ogniste Pióra', 'ogniste pióra', 'zuchy.szubin@wp.pl', 'zuchy.szubin@wp.pl', 1, 'alo9wmwn0204koog40wk0wwogkwkgs4', 'KTo3qpCaf7iJd/0KmBcd10QkGlp+GDUDC+wVW7Mx0PcXX3HL5pZtPk/LRuCnkDdn+OshJXKF7sHk8muVYnJWfg==', '2016-03-30 19:12:17', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Joanna', 'Lewandowska', '500-635-780', 'Hufiec Pałuki im. Olgi i Andrzeja Małkowskich, chorągiew kujawsko-pomorska, drużynowa, namiestnik zuchowy'),
(32, 'gduda', 'gduda', 'druzynowa29@tlen.pl', 'druzynowa29@tlen.pl', 1, '66yvuc03i60w40o4c8880s8ks448848', '4JjQ2fiMWIP4vPQL0AgOY4CNcU8zF/d9yMxaMy49nwDv74vZBBf+U/43ugLZhE839RS/3KmDc0a7WXBHYQO4ng==', '2016-03-31 22:49:00', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Grażyna', 'Duda', '608837666', 'Wyszków,\r\n Mazowiecka , \r\ndrużynowa'),
(33, 'Rudzielec', 'rudzielec', 'prumyk9@gmail.com', 'prumyk9@gmail.com', 1, 'hn3tbx76hq0w0oows4kgg80kocwg0g0', 'znlDHcnMCPl0lWQhJadRPlNJA+HCP247ciADjnxOuXf9c3lAC03ahrSFFR3vlZ416k4VyMEX+PQiYY5i2NfxNg==', '2016-03-27 20:48:51', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Przemysław', 'Chybicki', 'Rudzielec', 'Hufiec Warmiński \r\nChorągiew warmińsko-mazurska'),
(34, '2wdhzp', '2wdhzp', 'mmodzelesia@poczta.fm', 'mmodzelesia@poczta.fm', 1, 'ov0kyhrruk0sgckc0c8w4c88w4gsoc4', 'RwUjiCqtYdXv1eMpsa4SEQ0tODSeNlt+dYqnZgUfcXTgWwybqO2PeyCmE+lsby9IDj6Sxis1wFubeP3Atjofhg==', '2016-03-30 15:18:18', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Małgorzata', 'Modzelewska', '509586844', 'Nadnarwiański w Łomży\r\nBiałostocka\r\nharcmistrz'),
(35, 'Magdalena Ziuta Cieśluk', 'magdalena ziuta cieśluk', 'magdaciesluk96@gmail.com', 'magdaciesluk96@gmail.com', 1, 's9wx98seytc4kk48o4s84ogokow44so', 'yKAuq1BPZTY47B+8yuWYuwN2Ny6sC6Bi05lPHIjAPvQRB7f/mLHyZK90O9YjbUIROKthwCP1BsVndtQVU11pIg==', '2016-03-30 20:37:30', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Magdalena', 'Cieśluk', '517671687', 'Hufiec Białystok,\r\nChorągiew Białostocka\r\nprzyboczna 40GBDW'),
(36, 'ZWIERZAKI', 'zwierzaki', 'justawu@serwus.pl', 'justawu@serwus.pl', 1, 'akljudu3xc84g4wskkkwww40w00kow8', 'FFN9XTSkZAPLj3ye9Uu7pBAf0E0onJDMBHR1jycQww6l1e1WiXX/5UBSMOGVOklUdQOn3UwydUPTgUpGC9PVgA==', '2016-03-30 18:32:40', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Justyna', 'Witas', '696462717', 'Hufiec Beskidzki, Chorągiew Śląska, Drużynowa'),
(37, 'doma7', 'doma7', 'dominika.zak7@gmail.com', 'dominika.zak7@gmail.com', 1, '6zrzxkyo65gkks0w88k000o8s4okwko', 'ksdwELuby0hm/QtermcuG9tZhcnTVifQ8PwboauEckFiC4oIhv5BkxsDGP6XAIMx2d0rJrgQXNKdXX3n61+vqQ==', '2016-04-01 00:16:00', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Dominika', 'Zakrzewska', '605162902', 'Chorągiew Stołeczna, Hufiec Wołomin, Szczep 14 DH i GZ Nieporęt, drużynowa 14 Grunwaldzkiej DH "Puszcza"'),
(38, 'Tasia', 'tasia', '17wkdhbezimienni@gmail.com', '17wkdhbezimienni@gmail.com', 1, '3r7l26txy0e88s0wksow4gog44cwk0o', 'v2QdVm41w3Wj+mOW7+q68ov7ZJBDAeVesuMGGQWLETntb9RrM/EL/OBfuFkjLdzD2MdBmESWiW9wCe52FQLXqw==', '2016-04-05 09:01:37', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Natalia', 'Zielińska', '500140152', 'Hufiec Oborniki, Chorągiew Wielkopolska, komendantka Hufca'),
(39, 'Rzeźnik', 'rzeźnik', 'mateusz.wiejak@zhp.net.pl', 'mateusz.wiejak@zhp.net.pl', 1, 'b3th982uuzccscw88kw8o0sgs0cssw0', 'QUOaqouvims1LSbhFHmIidqYgGuluDPt649/UAlSh6wQ2tobyPR5ojp7mBzkbfE25MzY8r9BlPFN1/7UlvhUwA==', '2016-03-30 22:55:14', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Mateusz', 'Wiejak', '785 721 990', 'Hufiec Nidzica, Chorągiew Warmińsko-Mazurska, drużynowy'),
(40, 'Ewelina Różańska', 'ewelina różańska', 'ewelina.roz19@gmail.com', 'ewelina.roz19@gmail.com', 1, 'k5sj8ny29lw0sogcs4ksgcsg08c44wc', 'anvvdcaVEcb4viFpQXb9NChBtOCfaGtgxIrpSZyeakjdKC6saXHrpj2dmpZVDHU5MJLszdyLhm2su2E9FEWPfw==', '2016-03-31 09:13:07', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Ewelina', 'Różańska', '514945230', 'Hufiec Działdowo\r\nChorągiew Warmińsko-Mazurska'),
(41, 'lespatrycja123', 'lespatrycja123', 'lespatrycja@gmail.com', 'lespatrycja@gmail.com', 1, '4lgxnivdq5c0o4o888w04c0c8c4o8k4', 'SNB2WYgbZ9RXIXycGvqBp1hQ5pDwngHr4ohDiX+Btqs8ozdla+MqEORjBjCW1HJsu3XqA/lLg0cYYczmxvvzBg==', '2016-04-01 18:19:59', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Patrycja', 'Leś', '783296936', 'Beskidzki, Śląska, drużynowa'),
(42, '40GBDW', '40gbdw', 'daretzky69@wp.pl', 'daretzky69@wp.pl', 1, 'f935elfj9lc8os4ggo0o04ok4okww0o', 'H8vCm3Egq6Y6LSxCtxsg3csNzLHdoTo+9UrS9013gkp0fTQOZML4Ut1W+KGjU66m7vFE0BfVFHS8/Op/eLApcA==', '2016-03-31 22:16:52', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, 'Dariusz', 'Szehidewicz', '507162818', 'Białystok');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `invoice`
--

CREATE TABLE IF NOT EXISTS `invoice` (
  `id` int(11) NOT NULL,
  `team_id` int(11) DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `invoice`
--

INSERT INTO `invoice` (`id`, `team_id`, `full_name`, `street`, `city`, `nip`, `created`) VALUES
(1, 1, 'Chorągiew Śląska Hufiec ZHP Dąbrowa Górnicza', 'Królowej Jadwigii 8', '41-300 Dąbrowa Górnicza', '6340195483', '2016-02-29 21:58:52'),
(2, 8, 'VII Przyparafialna Wodna Grunwaldzka Żeńska Drużyna Starszoharcerska "Jungi"', 'ul. Stary Rynek 18', '06-500 Mława', '7743136686', '2016-03-09 16:47:36'),
(3, 11, 'Chorągiew Kujawsko-Pomorska ZHP Hufiec Świecie-Powiat', 'ul. Pod Murami 1', '86-170 Nowe', '9671254011', '2016-03-14 11:07:07'),
(4, 30, 'Chorągiew Mazowiecka ZHP Hufiec Ostrołęka', 'plac Bema 11', 'Ostrołęka, 07-410', '7743136686', '2016-03-28 20:28:38'),
(5, 31, 'Stowarzyszenie Inspiracja SI', 'Pałucka 2/19', 'Szubin, 89-200', '5581862911', '2016-03-30 19:03:35'),
(6, 39, 'Chorągiew Kujawsko-Pomorska ZHP Hufiec Świecie-Powiat', 'ul. Pod Murami 1', '86-170 Nowe', '9671254011', '2016-03-30 20:47:11'),
(7, 43, 'Związek Harcerstwa Polskiego  Chorągiew Warmińsko-Mazurska  Hufiec Nidzica ZHP', 'Pl. Wolności 1', 'Nidzica, 13-100', '7393591437', '2016-03-30 23:12:14'),
(8, 46, 'ZHP Chorągiew Białostocka', 'ul. Pałacowa 3/1', '15-042 Białystok', '5420211663', '2016-03-31 22:06:03'),
(9, 47, 'ZHP Chorągiew Białostocka', 'ul. Pałacowa 3/1', '15-042 Białystok', '5420211663', '2016-03-31 22:12:47');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `payment`
--

CREATE TABLE IF NOT EXISTS `payment` (
  `id` int(11) NOT NULL,
  `team_id` int(11) DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL,
  `information` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_date` datetime NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `payment`
--

INSERT INTO `payment` (`id`, `team_id`, `amount`, `information`, `payment_date`, `created`, `modified`) VALUES
(1, 11, 990.00, NULL, '2016-03-29 00:00:00', '2016-04-05 21:48:26', '2016-04-05 21:48:26');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `person`
--

CREATE TABLE IF NOT EXISTS `person` (
  `id` int(11) NOT NULL,
  `team_id` int(11) DEFAULT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pesel` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `shirt_size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `diet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `register_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `person_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `person`
--

INSERT INTO `person` (`id`, `team_id`, `first_name`, `last_name`, `address`, `pesel`, `shirt_size`, `diet`, `register_date`, `update_date`, `user_id`, `person_type`) VALUES
(1, 21, 'Monika', 'Chmiel', 'Morąska 36A/1, 11-130 Orneta', '88080809445', 'XL', 'Zwykła', '2016-03-29 09:47:02', '2016-03-29 09:47:02', 6, 'D'),
(2, 25, 'Magdalena', 'Cieśluk', 'ul. Dojlidy Górne 41, 15-572 Białystok', '96052603162', 'L', 'Zwykła', '2016-03-30 20:39:36', '2016-03-30 20:39:36', 35, 'D');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stat_application`
--

CREATE TABLE IF NOT EXISTS `stat_application` (
  `id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `z` int(11) DEFAULT NULL,
  `h` int(11) DEFAULT NULL,
  `hs` int(11) DEFAULT NULL,
  `w` int(11) DEFAULT NULL,
  `i` int(11) DEFAULT NULL,
  `o` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `stat_application`
--

INSERT INTO `stat_application` (`id`, `count`, `created`, `z`, `h`, `hs`, `w`, `i`, `o`) VALUES
(1, 31, '2016-03-01 00:00:00', 0, 10, 12, 6, 1, 2),
(2, 31, '2016-03-02 00:00:00', 0, 10, 12, 6, 1, 2),
(3, 35, '2016-03-03 00:00:00', 0, 14, 12, 6, 1, 2),
(4, 35, '2016-03-04 00:00:00', 0, 14, 12, 6, 1, 2),
(5, 35, '2016-03-05 00:00:00', 0, 14, 12, 6, 1, 2),
(6, 35, '2016-03-06 00:00:00', 0, 14, 12, 6, 1, 2),
(7, 54, '2016-03-07 00:00:00', 0, 25, 13, 11, 2, 3),
(8, 54, '2016-03-08 00:00:00', 0, 25, 13, 11, 2, 3),
(9, 54, '2016-03-09 00:00:00', 0, 25, 13, 11, 2, 3),
(10, 69, '2016-03-10 00:00:00', 0, 25, 24, 15, 2, 3),
(11, 69, '2016-03-11 00:00:00', 0, 25, 24, 15, 2, 3),
(12, 69, '2016-03-12 00:00:00', 0, 25, 24, 15, 2, 3),
(13, 69, '2016-03-13 00:00:00', 0, 25, 24, 15, 2, 3),
(14, 69, '2016-03-14 00:00:00', 0, 25, 24, 15, 2, 3),
(15, 79, '2016-03-14 00:00:00', 0, 31, 25, 17, 3, 3),
(16, 79, '2016-03-16 00:00:00', 0, 31, 25, 17, 3, 3),
(17, 79, '2016-03-17 00:00:00', 0, 31, 25, 17, 3, 3),
(18, 79, '2016-03-18 00:00:00', 0, 31, 25, 17, 3, 3),
(19, 79, '2016-03-19 00:00:00', 0, 31, 25, 17, 3, 3),
(20, 79, '2016-03-20 00:00:00', 0, 31, 25, 17, 3, 3),
(21, 79, '2016-03-21 00:00:00', 0, 31, 25, 17, 3, 3),
(22, 79, '2016-03-22 00:00:00', 0, 31, 25, 17, 3, 3),
(23, 79, '2016-03-23 00:00:00', 0, 31, 25, 17, 3, 3),
(24, 79, '2016-03-24 00:00:00', 0, 31, 25, 17, 3, 3),
(25, 79, '2016-03-25 00:00:00', 0, 31, 25, 17, 3, 3),
(26, 84, '2016-03-26 00:00:00', 4, 31, 25, 17, 3, 4),
(27, 84, '2016-03-27 00:00:00', 4, 31, 25, 17, 3, 4),
(28, 102, '2016-03-28 00:00:00', 4, 31, 28, 31, 4, 4),
(29, 124, '2016-03-29 00:00:00', 4, 31, 32, 46, 6, 5),
(30, 141, '2016-03-30 00:00:00', 6, 40, 33, 49, 7, 6),
(31, 260, '2016-03-31 00:00:00', 46, 57, 57, 78, 10, 12),
(32, 317, '2016-04-01 00:00:00', 56, 73, 70, 90, 14, 14),
(33, 317, '2016-04-02 00:00:00', 56, 73, 70, 90, 14, 14),
(34, 317, '2016-04-03 00:00:00', 56, 73, 70, 90, 14, 14),
(35, 317, '2016-04-04 00:00:00', 56, 73, 70, 90, 14, 14),
(36, 317, '2016-04-05 00:00:00', 56, 73, 70, 90, 14, 14);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `team`
--

CREATE TABLE IF NOT EXISTS `team` (
  `id` int(11) NOT NULL,
  `ensign_id_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `team_name` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `troops` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `is_grunwaldzka` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `team`
--

INSERT INTO `team` (`id`, `ensign_id_id`, `user_id`, `team_name`, `troops`, `is_grunwaldzka`, `created`, `status`) VALUES
(1, 12, 2, '4 Grunwaldzka Dąbrowska Drużyna Harcerska "Leśni ludzie" im. Janusza Korczaka', 'Dąbrowa Górnicza', 'tak', '2016-02-29 21:55:26', 1),
(2, 14, 3, '4 Grunwaldzka Drużyna Starszoharcerska "Feniks" im. Aleksego "Alka" Dawidowskiego', 'Bartoszyce', 'tak', '2016-02-29 22:36:37', 1),
(3, 14, 4, '34 Grunwaldzka Wielopoziomowa Drużyna Harcerska "Antidotum" z Dywit', 'Warmiński', 'tak', '2016-02-29 22:36:48', 1),
(4, 4, 5, '15 Grunwaldzka Drużyna Harcerska im. "Jędrusiów"', 'Starachowice', 'tak', '2016-02-29 22:47:24', 1),
(5, 15, 8, '9 Grunwaldzka Drużyna Harcerska "Buki" im. Kazimierza Wielkiego w Wielichowie', 'Hufiec ZHP Śmigiel', 'tak', '2016-03-01 10:22:30', 1),
(6, 14, 10, '20 Iławska Grunwaldzka Drużyna Harcerska "Żywioły"', 'Iława', 'tak', '2016-03-02 01:01:52', 1),
(7, 15, 12, '501 Poznańska Drużyna Harcerek "Aranea"', 'Poznań-Jeżyce', 'nie', '2016-03-02 16:13:38', 1),
(8, 9, 17, 'VII Przyparafialna Wodna Grunwaldzka Żeńska  Drużyna Starszoharcerska "Jungi"', 'Hufiec ZHP Mława', 'tak', '2016-03-08 14:14:56', 1),
(9, 2, 20, '12 Grunwaldzka Drużyna harcerska Bastion', 'Zgorzelec', 'tak', '2016-03-08 16:14:33', 1),
(10, 9, 22, '8 Ostrołęcka Grunwaldzka Drużyna Harcerska "Wilki" im. Jana Romockiego ps. " Bonawentura "', 'Ostrołęka', 'tak', '2016-03-09 18:25:49', 1),
(11, 6, 23, '26 Grunwaldzka Środowiskowa Drużyna Harcerska "Lisowczycy"', 'Świecie-Powiat', 'tak', '2016-03-14 10:49:46', 1),
(12, 13, 24, '14 GDH "Puszczyki"', 'Wołomin', 'tak', '2016-03-14 19:17:01', 1),
(13, 14, 27, '1 wdh ex bi-pi', 'pisz', 'nie', '2016-03-14 20:38:55', 0),
(14, 14, 26, '1 WDH Ex "Bi-Pi"', 'Pisz', 'nie', '2016-03-14 20:38:56', 1),
(16, 14, 1, 'Drużyna Kwatermistrzowska', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', 0),
(17, 14, 1, 'HSP', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', 0),
(18, 14, 1, 'Recepcja', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', 0),
(19, 14, 1, 'Zespół plastyka', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', 0),
(20, 14, 1, 'Zespół programowy', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', 0),
(21, 14, 1, 'Komenda Zlotu', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', 0),
(22, 14, 1, 'Grund Info', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', 0),
(23, 14, 1, 'Zespół Promocji', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', 0),
(24, 14, 1, 'Komenda Gniazda Warmińsko-Mazurskiego', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', 0),
(25, 14, 1, 'Komenda Gniazda Środkowa Polska', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', 0),
(26, 14, 1, 'Komenda Gniazda Śląskiego', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', 0),
(27, 14, 1, 'Komenda Gniazda Mazowieckiego', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', 0),
(28, 14, 1, 'Komenda Gniazda Wielkopolskiego', 'Drużyna techniczna', 'nie', '2016-01-01 12:34:56', 0),
(29, 14, 30, '87 Drużyna Harcerek "Nie z Tego Świata"', 'Orneta', 'nie', '2016-03-18 20:02:11', 1),
(30, 9, 16, '3 Zambrowska Drużyna Harcerska "Grota"', 'Ostrołęka', 'tak', '2016-03-19 10:19:24', 1),
(31, 6, 31, 'Grunwaldzka Gromada Zuchowa Ogniste Pióra', 'Hufiec Pałuki im. Olgi i Andrzeja Małkowskich', 'tak', '2016-03-19 15:32:11', 1),
(32, 9, 32, '29 Drużyna Harcerska "Pogodni"', 'WYSZKÓW', 'tak', '2016-03-23 17:30:56', 1),
(33, 14, 26, 'Gromada Zuchowa "Pluszowe Misiaki" z Dąbrówki', 'Pisz', 'nie', '2016-03-25 22:30:49', 1),
(34, 14, 26, '3 Żeńska Drużyna Harcerska "Amazonki" z Pisza', 'Pisz im. Janusza Korczaka', 'nie', '2016-03-25 22:36:46', 1),
(35, 7, 15, 'II Piotrkowska Drużyna Harcerska "Sulima" im. Zawiszy Czarnego', 'Piotrków Trybunalski', 'nie', '2016-03-27 15:58:23', 1),
(36, 19, 33, '15 kompania piechoty czarne pantery', 'inne', 'nie', '2016-03-27 20:49:52', 1),
(37, 14, 25, '26 Grunwaldzka Wielopoziomowa Olsztyńska Drużyna Harcerska ,,Cassiopea"', 'Warmiński', 'tak', '2016-03-27 22:29:29', 1),
(38, 1, 34, '2 Grunwaldzka Wielopoziomowa Drużyna Harcerska "Zielony Płomień" z Jedwabnego', 'Nadnarwiański w Łomży', 'tak', '2016-03-29 08:51:38', 1),
(39, 6, 23, 'Bosaki', 'Świecie-Powiat', 'nie', '2016-03-29 12:11:25', 1),
(40, 12, 36, '38 Grunwaldzka Beskidzka Gromada Zuchowa "ZWIERZAKI"', 'Beskidzki', 'tak', '2016-03-29 18:28:54', 1),
(41, 13, 37, '14 Grunwaldzka DH "Puszcza"', 'Wołomin', 'tak', '2016-03-29 22:00:36', 1),
(42, 15, 38, '17 Grunwaldzka Wielopoziomowa Koedukacyjna Drużyna Harcerska Bezimienni', 'Oborniki', 'tak', '2016-03-30 14:29:12', 1),
(43, 14, 39, '6 Grunwaldzka Nidzicka Drużyna Harcerska "Vitae" im. Floriana Marciniaka', 'Nidzica', 'tak', '2016-03-30 22:58:12', 1),
(44, 14, 40, 'Hufiec Działdowo', 'Działdowo', 'nie', '2016-03-31 09:15:56', 1),
(45, 12, 41, '10 Grunwaldzka Beskidzka Gromada Zuchowa "Wesoła Gromadka Kubusia Puchatka"', 'Beskidzki', 'tak', '2016-03-31 10:27:01', 1),
(46, 1, 42, '40 Grunwaldzka Białostocka Drużyna Wielopoziomowa', 'Białystok', 'tak', '2016-03-31 21:58:32', 1),
(47, 1, 42, 'Próbna Białostocka Gromada Zuchowa (44BGZ)', 'Białystok', 'tak', '2016-03-31 22:09:10', 1);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `application`
--
ALTER TABLE `application`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_A45BDDC1296CD8AE` (`team_id`);

--
-- Indexes for table `dict_ensign`
--
ALTER TABLE `dict_ensign`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_90651744296CD8AE` (`team_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_6D28840D296CD8AE` (`team_id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_34DCD1763931747B` (`pesel`),
  ADD KEY `IDX_34DCD176296CD8AE` (`team_id`),
  ADD KEY `IDX_34DCD176A76ED395` (`user_id`);

--
-- Indexes for table `stat_application`
--
ALTER TABLE `stat_application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_C4E0A61F6121245F` (`ensign_id_id`),
  ADD KEY `IDX_C4E0A61FA76ED395` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `application`
--
ALTER TABLE `application`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT dla tabeli `dict_ensign`
--
ALTER TABLE `dict_ensign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT dla tabeli `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT dla tabeli `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT dla tabeli `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT dla tabeli `person`
--
ALTER TABLE `person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT dla tabeli `stat_application`
--
ALTER TABLE `stat_application`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT dla tabeli `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `application`
--
ALTER TABLE `application`
  ADD CONSTRAINT `FK_A45BDDC1296CD8AE` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`);

--
-- Ograniczenia dla tabeli `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `FK_90651744296CD8AE` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`);

--
-- Ograniczenia dla tabeli `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `FK_6D28840D296CD8AE` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`);

--
-- Ograniczenia dla tabeli `person`
--
ALTER TABLE `person`
  ADD CONSTRAINT `FK_34DCD176296CD8AE` FOREIGN KEY (`team_id`) REFERENCES `team` (`id`),
  ADD CONSTRAINT `FK_34DCD176A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Ograniczenia dla tabeli `team`
--
ALTER TABLE `team`
  ADD CONSTRAINT `FK_C4E0A61F6121245F` FOREIGN KEY (`ensign_id_id`) REFERENCES `dict_ensign` (`id`),
  ADD CONSTRAINT `FK_C4E0A61FA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

DELIMITER $$
--
-- Zdarzenia
--
CREATE DEFINER=`root`@`localhost` EVENT `Count Statistics for application` ON SCHEDULE EVERY 1 DAY STARTS '2016-03-22 00:00:00' ON COMPLETION PRESERVE ENABLE DO INSERT INTO stat_application (`count`,`created`,`z`,`h`,`hs`,`w`,`i`,`o`)
SELECT SUM(`total_count`) , CURDATE() , SUM(`z`) , SUM(`h`) , SUM(`hs`) , SUM(`w`) , SUM(`i`) , SUM(`o`)
FROM `application`$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

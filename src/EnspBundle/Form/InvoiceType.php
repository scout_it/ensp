<?php

namespace EnspBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class InvoiceType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('fullName', NULL, array(
                    'label' => 'Pełna Nazwa:'))
                ->add('street', NULL, array(
                    'label' => 'Ulica:'))
                ->add('city', NULL, array(
                    'label' => 'Miasto, kod pocztowy:'))
                ->add('nip', NULL, array(
                    'label' => 'NIP:'))
                ->add('save', SubmitType::class, array(
                    'label' => 'Zapisz'));

    }


    /**
     * @return string
     */
    public function getName()
    {
        return 'ensp_invoice_edit';
    }

}

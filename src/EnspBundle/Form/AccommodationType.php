<?php

namespace EnspBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccommodationType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('MondayTuesday', CheckboxType::class, array(
            'label' => 'form.accommodation.MondayTuesday',
            'translation_domain' => 'EnspBundle',
            'required' => false,
        ));
        $builder->add('TuesdayWednesday', CheckboxType::class, array(
            'label' => 'form.accommodation.TuesdayWednesday',
            'translation_domain' => 'EnspBundle',
            'required' => false,
        ));
        $builder->add('WednesdayThursday', CheckboxType::class, array(
            'label' => 'form.accommodation.WednesdayThursday',
            'translation_domain' => 'EnspBundle',
            'required' => false,
        ));
        $builder->add('ThursdayFriday', CheckboxType::class, array(
            'label' => 'form.accommodation.ThursdayFriday',
            'translation_domain' => 'EnspBundle',
            'required' => false,
        ));
        $builder->add('FridaySaturday', CheckboxType::class, array(
            'label' => 'form.accommodation.FridaySaturday',
            'translation_domain' => 'EnspBundle',
            'required' => false,
        ));
        $builder->add('SaturdaySunday', CheckboxType::class, array(
            'label' => 'form.accommodation.SaturdaySunday',
            'translation_domain' => 'EnspBundle',
            'required' => false,
        ));
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EnspBundle\Entity\Accommodation',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ensp_accommodation_edit';
    }

}

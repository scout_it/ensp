<?php

namespace EnspBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\ProfileFormType as BaseType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class ProfileFormType extends BaseType {

    public function buildUserForm(FormBuilderInterface $builder, array $options) {
        parent::buildUserForm($builder, $options);

        $builder->add('name', NULL, array(
            'label' => 'Imię'));
        $builder->add('surname', NULL, array(
            'label' => 'Nazwisko'));
        $builder->add('phone', NULL, array(
            'label' => 'Telefon kontaktowy'));
        $builder->add('info', TextareaType::class, array(
            'label' => 'Hufiec, chorągiew, pełniona funkcja'));

    }

    public function getName() {
        return 'ensp_user_profile';
    }

}


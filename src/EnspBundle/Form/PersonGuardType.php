<?php

namespace EnspBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class PersonGuardType extends PersonType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('grade', NULL, array(
                'label' => 'Stopień Instruktorski:'))
            ->add('grade', ChoiceType::class, array(
                'choices' => array(
                    'przewodnik' => 'pwd',
                    'podharcmistrz' => 'phm',
                    'harcmistrz' => 'hm',
                    'brak' => ' '
                ),
                'label' => 'Stopień Instruktorski:'
            ))

            ->add('certificate', NULL, array(
                'label' => 'Nr zaświadczenia o ukończeniu Kursu Wychowawców:'))
            ->add('krs', DateType::class, array(
                'input' => 'datetime',
                'widget' => 'single_text',
                'label' => 'krs',
                'translation_domain' => 'EnspBundle',))
            ->add('save', SubmitType::class, array(
                'label' => 'Zapisz'));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ensp_person_guard';
    }

}

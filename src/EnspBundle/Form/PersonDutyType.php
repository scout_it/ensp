<?php

namespace EnspBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PersonDutyType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('team', EntityType::class, array(
                'class' => 'EnspBundle:Team',
                'label' => 'Służba',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.troops = :name')
                        ->setParameter('name', 'Drużyna techniczna');
                },
            ))
            ->add('firstName', NULL, array(
                'label' => 'Imię:'))
            ->add('lastName', NULL, array(
                'label' => 'Nazwisko:'))
            ->add('address', NULL, array(
                'label' => 'Adres:'))
            ->add('pesel', NULL, array(
                'label' => 'Numer PESEL:'))
            ->add('shirtSize', ChoiceType::class, array(
                'choices' => array(
                    'XS' => 'XS',
                    'S' => 'S',
                    'M' => 'M',
                    'L' => 'L',
                    'XL' => 'XL',
                    'XXL' => 'XXL'
                ),
                'label' => 'Rozmiar koszulki:'
            ))
            ->add('diet', ChoiceType::class, array(
                'choices' => array(
                    'Zwykła' => 'Zwykła',
                    'Wegetariańska' => 'Wegetariańska',
                ),
                'label' => 'Dieta:'
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Zapisz'));

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EnspBundle\Entity\Person'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ensp_person_duty';
    }

}

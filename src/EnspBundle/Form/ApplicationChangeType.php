<?php

namespace EnspBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ApplicationChangeType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('count', NULL, array(
                'label' => 'Początkowa liczba osób:',
                'disabled'=>'disabled'))
            ->add('totalCount', NULL, array(
                'label' => 'Ostateczna liczba osób:'))
            ->add('z', NULL, array(
                'label' => 'Zuchów:'))
            ->add('h', NULL, array(
                'label' => 'Harcerzy:'))
            ->add('hs', NULL, array(
                'label' => 'Harcerzy starszych:'))
            ->add('w', NULL, array(
                'label' => 'Wędrowników:'))
            ->add('i', NULL, array(
                'label' => 'Instruktorów:'))
            ->add('o', NULL, array(
                'label' => 'Opiekunów:'))
            ->add('save', SubmitType::class, array(
                'label' => 'Zapisz'));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ensp_application_change';
    }

}

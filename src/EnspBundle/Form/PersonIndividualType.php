<?php

namespace EnspBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class PersonIndividualType extends PersonType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('shirtSize', ChoiceType::class, array(
                'choices' => array(
                    'brak' => 'brak',
                    'XS' => 'XS',
                    'S' => 'S',
                    'M' => 'M',
                    'L' => 'L',
                    'XL' => 'XL',
                    'XXL' => 'XXL'
                ),
                'label' => 'Rozmiar koszulki:'
            ))
            ->add('accommodation', AccommodationType::class, array(
                'label' => 'Noclegi:'))
            ->add('dinner', DinnerType::class, array(
                'label' => 'Obiady:'))
            ->add('save', SubmitType::class, array(
                'label' => 'Zapisz'));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ensp_person_ind';
    }

}

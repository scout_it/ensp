<?php

namespace EnspBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class TeamType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('teamName', NULL, array(
                'label' => 'Pełna nazwa Drużyny:'))
            ->add('ensignId', EntityType::class, array(
                'class' => 'EnspBundle:DictEnsign',
                'choice_label' => 'ensignName',
                'label' => 'Chorągiew:'

            ))
            ->add('troops', NULL, array(
                'label' => 'Hufiec:'))
            // TODO - PR: Polinglish? :)
            ->add('isGrunwaldzka', ChoiceType::class, array(
                'choices' => array(
                    'Tak' => 'tak',
                    'Nie' => 'nie',
                ),
                'choices_as_values' => true,
                'label' => 'Czy drużyna jest Grunwaldzka?'
            ))
            ->add('save', SubmitType::class, array(
                'label' => 'Zapisz'));

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'EnspBundle\Entity\Team'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ensp_team';
    }

}

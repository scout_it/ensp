<?php
/**
 * Created by PhpStorm.
 * User: Piszu
 * Date: 26.04.2016
 * Time: 18:23
 */

namespace EnspBundle\Entity;

use Doctrine\ORM\EntityRepository;

class PaymentRepository extends EntityRepository
{
    public function getTeamBalance($teamid)
    {

        $sql = "SELECT SUM(e.amount) AS balance FROM EnspBundle:Payment e " .
            "WHERE e.team = ?1";
        
        return $this->getEntityManager()->createQuery($sql)->setParameter(1, $teamid)->getSingleScalarResult();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Piszu
 * Date: 25.01.2016
 * Time: 22:55
 */

namespace EnspBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Accommodation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $MondayTuesday = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $TuesdayWednesday = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $WednesdayThursday = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $ThursdayFriday = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $FridaySaturday = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $SaturdaySunday = 0;

    /**
     * @ORM\Column(type="datetime")
     */
    private $registerDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updateDate;

    /**
     * @ORM\PrePersist
     */
    public function setPrePersistAction()
    {
        $this->registerDate = new \DateTime();
        $this->updateDate = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setPreUpdateAction()
    {
        $this->updateDate = new \DateTime();
    }

    function __toString()
    {
        return strval($this->getId());
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getMondayTuesday()
    {
        return $this->MondayTuesday;
    }

    /**
     * @param mixed $MondayTuesday
     */
    public function setMondayTuesday($MondayTuesday)
    {
        $this->MondayTuesday = $MondayTuesday;
    }

    /**
     * @return mixed
     */
    public function getTuesdayWednesday()
    {
        return $this->TuesdayWednesday;
    }

    /**
     * @param mixed $TuesdayWednesday
     */
    public function setTuesdayWednesday($TuesdayWednesday)
    {
        $this->TuesdayWednesday = $TuesdayWednesday;
    }

    /**
     * @return mixed
     */
    public function getWednesdayThursday()
    {
        return $this->WednesdayThursday;
    }

    /**
     * @param mixed $WednesdayThursday
     */
    public function setWednesdayThursday($WednesdayThursday)
    {
        $this->WednesdayThursday = $WednesdayThursday;
    }

    /**
     * @return mixed
     */
    public function getThursdayFriday()
    {
        return $this->ThursdayFriday;
    }

    /**
     * @param mixed $ThursdayFriday
     */
    public function setThursdayFriday($ThursdayFriday)
    {
        $this->ThursdayFriday = $ThursdayFriday;
    }

    /**
     * @return mixed
     */
    public function getFridaySaturday()
    {
        return $this->FridaySaturday;
    }

    /**
     * @param mixed $FridaySaturday
     */
    public function setFridaySaturday($FridaySaturday)
    {
        $this->FridaySaturday = $FridaySaturday;
    }

    /**
     * @return mixed
     */
    public function getSaturdaySunday()
    {
        return $this->SaturdaySunday;
    }

    /**
     * @param mixed $SaturdaySunday
     */
    public function setSaturdaySunday($SaturdaySunday)
    {
        $this->SaturdaySunday = $SaturdaySunday;
    }

    /**
     * @return mixed
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * @param mixed $registerDate
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;
    }

    /**
     * @return mixed
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param mixed $updateDate
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;
    }

}
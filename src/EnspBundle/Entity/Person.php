<?php
/**
 * Created by PhpStorm.
 * User: Piszu
 * Date: 25.01.2016
 * Time: 22:55
 */

namespace EnspBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="EnspBundle\Entity\PersonRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"pesel"},
 *     message="Osoba z takim numerem PESEL jest już zarejestrowana"
 * )
 */
class Person
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Team")
     */
    private $team;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", nullable = false)
     */
    private $address;

    // TODO - PR: No the best idea to put here all logic, it will be better to place all validation into dedicated yml file
    /**
     * @ORM\Column(type="string", length=11, nullable = false, unique=true)
     * @Assert\Regex(
     *      pattern = "/^([0-9][0-9]*)$/",
     *      message = "PESEL musi składać się z cyfr"
     * )
     * @Assert\Length(
     *      min = 11,
     *      max = 11,
     *      exactMessage = "PESEL musi zaiwerać 11 znaków"
     * )
     */
    private $pesel;

    /**
     * @ORM\Column(type="string", nullable = false)
     */
    private $shirtSize = 'brak';

    /**
     * @ORM\Column(type="string", nullable = false)
     */
    private $diet;

    /**
     * @ORM\Column(type="string", nullable = false)
     */
    private $personType = 'P';

    /**
     * @ORM\OneToOne(targetEntity="Accommodation",cascade={"persist"})
     */
    private $accommodation;

    /**
     * @ORM\OneToOne(targetEntity="Dinner",cascade={"persist"})
     */
    private $dinner;

    /**
     * @ORM\Column(type="string", nullable = true)
     */
    private $notes;

    /**
     * @ORM\Column(type="string", nullable = true)
     */
    private $certificate;

    /**
     * @ORM\Column(type="string", nullable = true)
     */
    private $grade;

    /**
     * @ORM\Column(type="datetime", nullable = true)
     */
    private $krs;

    /**
     * @ORM\Column(type="datetime")
     */
    private $registerDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updateDate;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable = true)
     */
    private $user;

    /**
     * @ORM\PrePersist
     */
    public function setPrePersistAction()
    {
        $this->registerDate = new \DateTime();
        $this->updateDate = new \DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function setPreUpdateAction()
    {
        $this->updateDate = new \DateTime();
    }

    public function __toString()
    {
        return strval($this->getFirstName());
    }

    /*
    *
    * Getters and setters
    *
    */

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param Team $team
     */
    public function setTeam(Team $team)
    {
        $this->team = $team;
    }


    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = ucwords(strtolower($firstName));
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = ucwords(strtolower($lastName));
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getPesel()
    {
        return $this->pesel;
    }

    /**
     * @param mixed $pesel
     */
    public function setPesel($pesel)
    {
        $this->pesel = $pesel;
    }

    /**
     * @return mixed
     */
    public function getShirtSize()
    {
        return $this->shirtSize;
    }

    /**
     * @param mixed $shirtSize
     */
    public function setShirtSize($shirtSize)
    {
        $this->shirtSize = $shirtSize;
    }

    /**
     * @return mixed
     */
    public function getDiet()
    {
        return $this->diet;
    }

    /**
     * @param mixed $diet
     */
    public function setDiet($diet)
    {
        $this->diet = $diet;
    }

    /**
     * @return mixed
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * @param mixed $registerDate
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;
    }

    /**
     * @return mixed
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param mixed $updateDate
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;
    }

    /**
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param int $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getPersonType()
    {
        return $this->personType;
    }

    /**
     * @param mixed $personType
     */
    public function setPersonType($personType)
    {
        $this->personType = $personType;
    }

    /**
     * @return Accommodation
     */
    public function getAccommodation()
    {
        return $this->accommodation;
    }

    /**
     * @param Accommodation $accommodation
     */
    public function setAccommodation(Accommodation $accommodation)
    {
        $this->accommodation = $accommodation;
    }

    /**
     * @return Dinner
     */
    public function getDinner()
    {
        return $this->dinner;
    }

    /**
     * @param Dinner $dinner
     */
    public function setDinner(Dinner $dinner)
    {
        $this->dinner = $dinner;
    }


    /**
     * @return mixed
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param mixed $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return mixed
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * @param mixed $grade
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;
    }

    /**
     * @return mixed
     */
    public function getCertificate()
    {
        return $this->certificate;
    }

    /**
     * @param mixed $certificate
     */
    public function setCertificate($certificate)
    {
        $this->certificate = $certificate;
    }

    /**
     * @return mixed
     */
    public function getKrs()
    {
        return $this->krs;
    }

    /**
     * @param mixed $krs
     */
    public function setKrs($krs)
    {
        $this->krs = $krs;
    }

}
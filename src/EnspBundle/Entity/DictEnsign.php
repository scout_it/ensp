<?php
/**
 * Created by PhpStorm.
 * User: Piszu
 * Date: 29.01.2016
 * Time: 18:16
 */

namespace EnspBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class DictEnsign
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=120)
     */
    private $ensignName;

    public function __toString()
    {
        return strval( $this->getEnsignName() );
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEnsignName()
    {
        return $this->ensignName;
    }

    /**
     * @param string $ensignName
     */
    public function setEnsignName($ensignName)
    {
        $this->ensignName = $ensignName;
    }



}

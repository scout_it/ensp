<?php
/**
 * Created by PhpStorm.
 * User: Piszu
 * Date: 03.02.2016
 * Time: 21:21
 */

namespace EnspBundle\Controller;

use EnspBundle\Entity\Application;
use EnspBundle\Form\ApplicationChangeType;
use EnspBundle\Form\ApplicationFullType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session;

// TODO: PR - Try to keep same naming convention for methods, read more
// https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-1-basic-coding-standard.md
// http://www.php-fig.org/psr/psr-2/

class ApplicationController extends Controller
{
    public function EditFullAction(Request $request, $teamid, $appid)
    {
        // TODO: PR - Magic numbers, you can use default valuse in Entity or Form, or move them to config!
       // static $AMOUNT_PER_ONE = 165.00;
        static $AMOUNT_PER_ONE = 170.00;

        static $ADVENCE_AMOUNT = 85.00;

        $man = $this->getDoctrine()->getManager();
        $team = $man->getRepository('EnspBundle:Team')->find($teamid);

        if ($team->getUser() != $this->getUser()) {
            return $this->redirect($this->generateUrl('ensp_team_list') . '?msg=failedAuth');
        }

        if ($appid == -1) {
            $application = new Application();
            $application->setTeam($team);
        } else {
            $application = $man->getRepository('EnspBundle:Application')->find($appid);
        }

        $appForm = $this->createForm(ApplicationFullType::class, $application);
        $appForm->handleRequest($request);

        if ($appForm->isSubmitted() && $appForm->isValid()) {

            $application->setTotalCount($application->getCount());
            $application->setAmountPerOne($AMOUNT_PER_ONE);
            $application->setTotalAmount($AMOUNT_PER_ONE * $application->getTotalCount());
            $application->setAdvance($application->getTotalCount() * $ADVENCE_AMOUNT);

            $man->persist($application);
            $man->flush();

            $this->sendEmail($application);

            return $this->redirect($this->generateUrl('ensp_team_view', array('teamid' => $teamid)) . '?msg=savedAppl');
        } else {
            return $this->render('EnspBundle:Ensp:app_edit.html.twig', array(
                'form' => $appForm->createView(),
                'a' => $application
            ));
        }

    }

    private function sendEmail($application)
    {
        $team = $application->getTeam();
        // TODO: PR - Good habit is to keep all strings as const strings, so you will be able to translate them later (if needed)
        $message = \Swift_Message::newInstance()
            ->setSubject('Potwierdzenie zgłoszenia - Zlot Grunwaldzki 2016')
            ->setFrom('zlotgrunwaldzki2016@gmail.com')
            ->setTo($team->getUser()->getEmail())
            ->setBody(
                $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'EnspBundle:Emails:app_register_confirm.html.twig',
                    array('appl' => $application,
                        'team' => $team)
                ),
                'text/html'
            );
        $this->get('mailer')->send($message);

    }

    public function ChangeAction(Request $request, $teamid, $appid)
    {

        $man = $this->getDoctrine()->getManager();
        $team = $man->getRepository('EnspBundle:Team')->find($teamid);

        if ($team->getUser() != $this->getUser()) {
            return $this->redirect($this->generateUrl('ensp_team_list'));
        }

        $application = $man->getRepository('EnspBundle:Application')->find($appid);

        $appForm = $this->createForm(ApplicationChangeType::class, $application);
        $appForm->handleRequest($request);

        if ($appForm->isSubmitted() && $appForm->isValid()) {
            $application->setTotalAmount($application->getAmountPerOne() * $application->getTotalCount());
            $man->persist($application);
            $man->flush();

            return $this->redirect($this->generateUrl('ensp_team_view', array('teamid' => $teamid)) . '?msg=savedChangeAppl');
        } else {
            return $this->render('EnspBundle:Ensp:app_change.html.twig', array(
                'form' => $appForm->createView(),
                'a' => $application
            ));
        }
    }

}
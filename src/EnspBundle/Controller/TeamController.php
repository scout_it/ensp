<?php
/**
 * Created by PhpStorm.
 * User: Piszu
 * Date: 10.12.2015
 * Time: 23:24
 */

namespace EnspBundle\Controller;

use EnspBundle\Entity\Application;
use EnspBundle\Entity\Team;
use EnspBundle\Form\TeamType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session;

class TeamController extends Controller
{
    public function IndexAction()
    {
        $user = $this->getUser();
        $teams = $this->getDoctrine()->getRepository('EnspBundle:Team')->findBy(['status' => 1, 'user' => $user]);
        return $this->render('EnspBundle:Ensp:team_list.html.twig', array('teams' => $teams));
    }

    public function IndexAdminAction()
    {
        $teams = $this->getDoctrine()->getRepository('EnspBundle:Team')->teamSummary();
        return $this->render('EnspBundle:Ensp:team_list_all.html.twig', array('teams' => $teams));
    }

    public function EditAction(Request $request, $teamid)
    {
        $man = $this->getDoctrine()->getManager();

        if ($teamid == -1) {
            $team = new Team();
            $team->setStatus(1); // TODO: PR - Move to entity constructor!
            $user = $this->getUser();
            $team->setUser($user);
            $team->setCreated(new \DateTime()); // TODO: PR - Move to entity constructor!
        } else {
            $team = $man->getRepository('EnspBundle:Team')->find($teamid);

            // TODO: PR - It should be outside this if maybe?
            // check user
            if ($team->getUser() != $this->getUser()) {
                return $this->redirect($this->generateUrl('ensp_team_list'));
            }

        }

        $teamForm = $this->createForm(TeamType::class, $team);
        $teamForm->handleRequest($request);

        if ($teamForm->isSubmitted() && $teamForm->isValid()) {
            $man->persist($team);
            $man->flush();

            if ($teamid == -1) {
                return $this->redirect($this->generateUrl('ensp_team_list') . '?msg=saved');
            } else {

                // TODO: PR - There is a special flashbag for this kind of messages! Generating this kinf of url make it unable to cache it later :)
                // read more about flashbag: http://symfony.com/doc/current/components/http_foundation/sessions.html
                return $this->redirect($this->generateUrl('ensp_team_view', ['teamid' => $teamid]) . '?msg=updated');
            }

        } else {
            return $this->render('EnspBundle:Ensp:team_edit.html.twig', array(
                'form' => $teamForm->createView(),
                'a' => $team
            ));
        }

    }

    public function ViewAction($teamid)
    {
        $team = $this->getDoctrine()->getRepository('EnspBundle:Team')->findOneBy(['id' => $teamid]);

        // check user
        if ($team->getUser() != $this->getUser()) {
            return $this->redirect($this->generateUrl('ensp_team_list'));
        }

        $appl = $this->getDoctrine()->getRepository('EnspBundle:Application')->findOneBy(['team' => $teamid]);
        $inv = $this->getDoctrine()->getRepository('EnspBundle:Invoice')->findOneBy(['team' => $teamid]);
        $payments = $this->getDoctrine()->getRepository('EnspBundle:Payment')->findBy(['team' => $teamid]);
        $persons = $this->getDoctrine()->getRepository('EnspBundle:Person')->findBy(['team' => $teamid]);
//
        $teamBalance = $this->getDoctrine()->getRepository('EnspBundle:Payment')->getTeamBalance($teamid);
//
        return $this->render('EnspBundle:Ensp:team_view.html.twig', array(
            'team' => $team,
            'a' => $team,
            'appl' => $appl,
            'inv' => $inv,
            'payments' => $payments,
            'teamBalance' => $teamBalance,
            'persons' => $persons
        ));
    }

    public function ViewAdminAction($teamid)
    {
        $team = $this->getDoctrine()->getRepository('EnspBundle:Team')->findOneBy(['id' => $teamid]);

        // check user
        if ($this->getUser()->hasRole('ROLE_PAY') != true) {
            return $this->redirect($this->generateUrl('ensp_team_list'));
        }

        $appl = $this->getDoctrine()->getRepository('EnspBundle:Application')->findOneBy(['team' => $teamid]);
        $inv = $this->getDoctrine()->getRepository('EnspBundle:Invoice')->findOneBy(['team' => $teamid]);
        $payments = $this->getDoctrine()->getRepository('EnspBundle:Payment')->findBy(['team' => $teamid]);
        $persons = $this->getDoctrine()->getRepository('EnspBundle:Person')->findBy(['team' => $teamid]);

        $teamBalance = $this->getDoctrine()->getRepository('EnspBundle:Payment')->getTeamBalance($teamid);

        $user = $team->getUser();

        return $this->render('EnspBundle:Ensp:team_view_adm.html.twig', array(
            'team' => $team,
            'a' => $team,
            'appl' => $appl,
            'inv' => $inv,
            'owner' => $user,
            'payments' => $payments,
            'teamBalance' => $teamBalance,
            'persons' => $persons
        ));
    }

    public function ToPrintAction($teamid)
    {

        $team = $this->getDoctrine()->getRepository('EnspBundle:Team')->findOneBy(['id' => $teamid]);

        // check user
        if ($team->getUser() != $this->getUser()) {
            return $this->redirect($this->generateUrl('ensp_team_list'));
        }

        /** @var Application $appl */
        $appl = $this->getDoctrine()->getRepository('EnspBundle:Application')->findOneBy(['team' => $teamid]);
        $inv = $this->getDoctrine()->getRepository('EnspBundle:Invoice')->findOneBy(['team' => $teamid]);
        $payments = $this->getDoctrine()->getRepository('EnspBundle:Payment')->findBy(['team' => $teamid]);
        $persons = $this->getDoctrine()->getRepository('EnspBundle:Person')->findBy(['team' => $teamid]);
        $teamBalance = $this->getDoctrine()->getRepository('EnspBundle:Payment')->getTeamBalance($teamid);
        $user = $team->getUser();
/*
        $html = $this->renderView('EnspBundle:EnspApiBundle:team_person_list_to_print.html.twig', array(
            'team' => $team,
            'a' => $team,
            'appl' => $appl,
            'inv' => $inv,
            'owner' => $user,
            'payments' => $payments,
            'teamBalance' => $teamBalance,
            'persons' => $persons
        ));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="file.pdf"'
            )
        );
*/

        return $this->render('EnspBundle:Ensp:team_person_list_to_print.html.twig', array(
            'team' => $team,
            'a' => $team,
            'appl' => $appl,
            'inv' => $inv,
            'owner' => $user,
            'payments' => $payments,
            'teamBalance' => $teamBalance,
            'persons' => $persons
        ));
        
    }


    public function DeleteAction($teamid)
    {
        $man = $this->getDoctrine()->getManager();
        $team = $man->getRepository('EnspBundle:Team')->findOneBy(['id' => $teamid]);
        $team->setStatus(0);
        $man->persist($team);
        $man->flush();
        return $this->redirect($this->generateUrl('ensp_team_list') . '?msg=deleted');
    }
}
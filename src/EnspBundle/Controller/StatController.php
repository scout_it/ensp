<?php
/**
 * Created by PhpStorm.
 * User: Piszu
 * Date: 03.02.2016
 * Time: 21:21
 */

namespace EnspBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session;

class StatController extends Controller
{

    public function IndexStatAction(Request $request)
    {
        $man = $this->getDoctrine()->getManager();
        // TODO: PR - For custom query there should be a dedicated reposytory manager
        $dql = "SELECT SUM(e.totalCount) AS totalCount FROM EnspBundle\Entity\Application e";
        $totalCount = $man->createQuery($dql)
            ->getSingleScalarResult();

        return $this->render('EnspBundle:Stats:stats_total.html.twig', array(
            'totalCount' => $totalCount
        ));
    }


}
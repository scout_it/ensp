<?php

namespace EnspBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

// TODO: PR - It's bad habit to leave default files, like this one.
class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('EnspBundle:Default:main.html.twig');
    }
}

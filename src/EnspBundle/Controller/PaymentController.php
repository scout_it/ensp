<?php
/**
 * Created by PhpStorm.
 * User: Piszu
 * Date: 03.02.2016
 * Time: 21:21
 */

namespace EnspBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session;
use EnspBundle\Entity\Payment;
use EnspBundle\Form\PaymentType;

class PaymentController extends Controller
{
    public function EditAction(Request $request, $teamid, $payid)
    {
        $payment = null; // TODO: PR - why?
        $man = $this->getDoctrine()->getManager();
        $team = $man->getRepository('EnspBundle:Team')->find($teamid);

        // check user
        if ($this->getUser()->hasRole('ROLE_PAY') != true) {
            return $this->redirect($this->generateUrl('ensp_team_list'));
        }
        
        if ($payid == -1) {
            $payment = new Payment();
            $payment->setCreated(new \DateTime()); // TODO: PR - Move to entity life cycle!
            $payment->setModified(new \DateTime()); // TODO: PR - Move to entity life cycle!
            $payment->setTeam($team);
        } else {
            $payment = $man->getRepository('EnspBundle:Payment')->find($payid);
            $payment->setModified(new \DateTime());
        }

        $invForm = $this->createForm(PaymentType::class, $payment);
        $invForm->handleRequest($request);

        if ($request->getMethod() == 'POST') {
            if ($invForm->isSubmitted() && $invForm->isValid()) {
                $man->persist($payment);
                $man->flush();


                $this->get('ensp.mailer')->registerPaymentMail($team, $payment);
               // $this->sendEmail($man, $payment);

                return $this->redirect($this->generateUrl('adm_ensp_team_view', array('teamid' => $teamid)) . '?msg=savedPay');
            } else {
                return $this->redirect($this->generateUrl('adm_ensp_team', array('teamid' => $teamid)) . '?msg=failed');
            }
        }

        return $this->render('EnspBundle:Ensp:payment_edit.html.twig', array(
            'form' => $invForm->createView(),
            'a' => $payment
        ));
    }
}
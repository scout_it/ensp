<?php

namespace Ensp\ApiBundle\Controller;

use EnspBundle\Entity\Person;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $data = json_decode($_POST['data'], true);

        // check varibles
        if(!isset($data['name'])  || !isset($data['surname'])){
            return new Response(-1, 200);
        }

        // check person exist in database
        $couts = $this->getDoctrine()->getRepository('EnspBundle:Person')->isPersonExist(ucwords($data['name']), ucwords($data['surname']));

        // return Response
        return new Response($couts, 200);
    }

}
